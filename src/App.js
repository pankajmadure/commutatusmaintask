import React, { Component } from 'react';
import './App.css';
import Header from './screens/Header'
import Sidebar from './screens/Sidebar'
import { Row, Col } from 'antd';
import 'antd/dist/antd.css';

import Opportunities from './screens/Opportunities';



class App extends Component {
  constructor(props) {
    super(props);
    this.state={
      clickedItem:"sfsdf"
    }
  }
  render() {
    return (
      <div className="App">

        <Header />
        <Row>
          <Col sm={5} style={{ borderRight: "2px solid gray" }} >
            <div>
              <Sidebar recievedData={(item)=>{
                 this.setState({
                   clickedItem:item
                 })
              }} />
            </div>
          </Col>
          <Col sm={19}>

            <Opportunities 
             selectedItem={this.state.clickedItem}
            
            />
          </Col>
        </Row>

      </div>
    )
  }
}

export default App;
