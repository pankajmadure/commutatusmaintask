import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import '../App.css'
import "react-datepicker/dist/react-datepicker.css";
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import { Row, Col, Button } from 'antd';
import Axios from 'axios';

const styles = {

    textStyle: {
        width: '80%',
        padding: 10
    }

}
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            minWidth: '300',
        },
    },
};
export default class Opportunities extends Component {

    constructor(props) {
        super(props);
        console.log("pankaj", this.props.selectedItem)
        this.state = {
            earliest_start_date: new Date(),
            application_close_date: "",
            latest_end_date: "",
            buttonName: "save",
            data: {

                "id": 6125,
                "title": "Test124",
                "lat": "13.0826802",
                "lng": "80.2707184",
                "url": "https://gis.aiesec.org/v2/opportunities/6125",
                "location": "Chennai, India",
                "status": "draft",
                "description": "Test Description 10",
                "is_gep": false,
                "current_status": "draft",
                "duration_min": null,
                "duration_max": null,
                "duration": 36,
                "project_duration": 8,
                "google_place_id": "ChIJ5YQQf1GHhYARPKG7WLIaOko",
                "opportunity_cost": {
                    "programme_fee": 430,
                    "project_fee": 5.97,
                    "total": 435.97,
                    "currency": "GBP"
                },
                "video_url": null,
                "transparent_fee_details": {
                    "sponsored_by": null,
                    "covers_pickup": null,
                    "covers_accomodation": null,
                    "covers_leadership_spaces": null,
                    "covers_administrative_costs": null
                },
                "is_ge": false,
                "programmes": {
                    "id": 1,
                    "short_name": "GV"
                },
                "sub_product": null,
                "is_favourited": false,
                "favourites": 0,
                "remark": null,
                "applied_to": true,
                "applied_to_with": 5271,
                "earliest_start_date": "2018-12-03T00:00:00Z",
                "latest_end_date": "2019-08-17T00:00:00Z",
                "applications_close_date": "2019-08-20T00:00:00Z",
                "applications_count": 1,
                "opened_by": null,
                "latest_reviews": [],
                "office_footfall_for_exchange": 0,
                "host_lc": {
                    "id": 630,
                    "name": "Chennai",
                    "full_name": "AIESEC in Chennai",
                    "email": null,
                    "tag": "LC",
                    "parent_id": 1585,
                    "signup_link": null,
                    "country": "India",
                    "url": "https://gis.aiesec.org/v2/committees/630",
                    "profile_photo_url": "https://cdn-expa.aiesec.org/gis-img/missing_profile_c.svg",
                    "cover_photo_url": "https://cdn-expa.aiesec.org/gis-img/missing_office_cover.png"
                },
                "home_lc": {
                    "id": 630,
                    "name": "Chennai",
                    "full_name": "AIESEC in Chennai",
                    "email": null,
                    "tag": "LC",
                    "parent_id": 1585,
                    "signup_link": null,
                    "country": "India",
                    "url": "https://gis.aiesec.org/v2/committees/630",
                    "profile_photo_url": "https://cdn-expa.aiesec.org/gis-img/missing_profile_c.svg",
                    "cover_photo_url": "https://cdn-expa.aiesec.org/gis-img/missing_office_cover.png"
                },
                "managers": [],
                "branch": {
                    "id": 3220,
                    "name": "Chennai",
                    "scope": "global",
                    "status": "approved",
                    "organisation": {
                        "id": 1754,
                        "name": "Laurin",
                        "meta": {
                            "video_url_id": "",
                            "employee_count": "3",
                            "opportunity_count": "1"
                        },
                        "url": "https://gis.aiesec.org/v2/organisations/1754",
                        "website": null,
                        "summary": "It's pretty great",
                        "is_gep": false,
                        "registration_no": null,
                        "experience_count": null,
                        "year_of_creation": 2012,
                        "contact_info": {
                            "website": null,
                            "email": null,
                            "phone": "12345",
                            "facebook": "",
                            "twitter": "",
                            "instagram": "",
                            "linkedin": "",
                            "country_code": "213"
                        },
                        "profile_photo_urls": {
                            "original": "https://cdn-expa.aiesec.org/gis-img/missing_profile_l.svg",
                            "medium": "https://cdn-expa.aiesec.org/gis-img/missing_profile_l.svg",
                            "thumb": "https://cdn-expa.aiesec.org/gis-img/missing_profile_l.svg"
                        },
                        "cover_photo_urls": "https://cdn-expa.aiesec.org/gis-img/missing_company_cover.png"
                    },
                    "address_info": {
                        "address_1": "Somewhere",
                        "address_2": "",
                        "postcode": "",
                        "city": "Chennai",
                        "country": null,
                        "city_name": null,
                        "country_name": null
                    },
                    "billing_address": null,
                    "url": "https://gis.aiesec.org/v2/organisations//branches/3220",
                    "contact_info": null
                },
                "team": null,
                "project": null,
                "tn_manager_info": null,
                "openings": 1,
                "available_openings": 1,
                "nationalities": [],
                "skills": [],
                "backgrounds": ['red', 'blue', 'green'],
                "languages": [],
                "measure_of_impacts": [],
                "issues": [],
                "study_levels": [],
                "preferred_locations_info": [],
                "logistics_info": {
                    "food_covered": "Not covered",
                    "food_weekends": "false",
                    "accommodation_covered": "false",
                    "accommodation_provided": "Provided"
                },
                "tm_details": {},
                "legal_info": {
                    "visa_link": "https://google.de",
                    "visa_type": "This is a test opportunity. Don't apply.",
                    "visa_duration": "This is a test opportunity. Don't apply.",
                    "health_insurance_info": "This is a test opportunity. Don't apply."
                },
                "role_info": {
                    "city": "Chennai",
                    "required_preparation": null,
                    "learning_points": null,
                    "learning_points_list": [
                        "This is a test opportunity. Don't apply .",
                        "This is a test opportunity. Don't apply.",
                        "This is a test opportunity. Don't apply.",
                        "This is a test opportunity. Don't apply.",
                        "This is a test opportunity. Don't apply."
                    ],
                    "additional_instructions": null,
                    "selection_process": "Pre-final round"
                },
                "department": null,
                "specifics_info": {
                    "salary": "100555555",
                    "salary_currency": null,
                    "ask_gip_question": null,
                    "salary_periodicity": null,
                    "ef_test_required": "false",
                    "computer": false,
                    "saturday_work": false,
                    "expected_work_schedule": {
                        "from": "1",
                        "to": "14"
                    },
                    "saturday_work_schedule": null
                },
                "sdg_info": {
                    "id": 2040,
                    "partners": null,
                    "coordination": null,
                    "implementation": null,
                    "potential_reach": null,
                    "impact_date": null,
                    "already_reached": null,
                    "location": null,
                    "deliverables": [],
                    "sdg_target": {
                        "id": 139,
                        "target": "Significantly reduce all forms of violence and related death rates everywhere",
                        "parent_id": 11116,
                        "description": "Significantly reduce all forms of violence and related death rates everywhere",
                        "goal_id": 11116,
                        "goal_index": 16,
                        "target_index": 2
                    },
                    "created_at": "2018-04-10T12:47:54Z",
                    "updated_at": "2018-05-14T13:25:14Z"
                },
                "nps_score": 0,
                "reviews": {
                    "score": 0,
                    "count": 0
                },
                "comments": [],
                "scorecard": null,
                "profile_photo_urls": {
                    "original": "https://cdn-expa.aiesec.org/gis-img/missing_profile_t.svg",
                    "medium": "https://cdn-expa.aiesec.org/gis-img/missing_profile_t.svg",
                    "thumb": "https://cdn-expa.aiesec.org/gis-img/missing_profile_t.svg"
                },
                "cover_photo_urls": "https://cdn-expa.aiesec.org/gis-img/gv_default.png",
                "permissions": {
                    "can_update": true,
                    "can_edit_managers": true,
                    "can_open": true,
                    "can_be_applied_to": true,
                    "view_applications": true,
                    "can_mark_gep": true
                },
                "created_at": "2018-04-10T12:47:54Z",
                "updated_at": "2019-06-04T11:07:35Z"

            },
            disabled: true,
            data1: this.props.selectedItem,
            backgrounds: []
        }


    }
    componentDidUpdate(prevProps, updatedProps) {


        if (prevProps.selectedItem != this.props.selectedItem) {
            this.setState({
                data: this.props.selectedItem
            })
        }
        else {

        }



    }

    componentDidMount() {
        //http://gisapi-web-staging-1636833739.eu-west-1.elb.amazonaws.com/v2/lists/backgrounds
        var params = {
            access_token: "dd0df21c8af5d929dff19f74506c4a8153d7acd34306b9761fd4a57cfa1d483c"
        }
        Axios.get("http://gisapi-web-staging-1636833739.eu-west-1.elb.amazonaws.com/v2/lists/backgrounds", { params }
        )
            .then(res => {
                console.log("Opportunities",res.data)

                this.setState({
                    application_close_date: new Date(this.state.data.applications_close_date),
                    earliest_start_date: new Date(this.state.data.earliest_start_date),
                    latest_end_date: new Date(this.state.data.latest_end_date),
                    backgrounds: res.data,
                    isLoading: false
                })
            })
            .catch(err => {
                console.log("Some problem with network ");
                this.setState({
                    application_close_date: new Date(this.state.data.applications_close_date),
                    earliest_start_date: new Date(this.state.data.earliest_start_date),
                    latest_end_date: new Date(this.state.data.latest_end_date),
                    backgrounds: [],
                    isLoading: false
                })
            })






    }
    saveData() {

        Axios.post("url", { "params": "" })
            .then(res => {

            })
            .catch(err => {
                console.log("error in storing data");
            })

        this.setState({
            buttonName: "save",
            disabled: true
        })
    }
    render() {

        return (

            <div >
                {this.state.buttonName == "save"
                    ?
                    <Button type="link" onClick={() => {
                        this.setState({
                            buttonName: "edit",
                            disabled: false
                        })
                    }}>
                        edit
                   </Button>
                    :
                    <Button type="link" onClick={this.saveData.bind(this)}>
                        save
                    </Button>

                }



                <div className={"App-header"}>

                    <TextField
                        disabled={this.state.disabled}
                        id="outlined-disabled"
                        value={this.state.data.title}
                        label="Title"
                        style={styles.textStyle}
                        margin="normal"
                        variant="outlined"
                        onChange={(event) => {

                            const max_char = 100;
                            if (event.target.value.length > max_char) {
                                event.target.value = event.target.value.substr(0, max_char);
                            }
                        }}
                    />
                    <DatePicker
                        disabled={this.state.disabled}
                        onChange={(item) => { this.setState({ application_close_date: new Date(item) }) }}
                        selected={this.state.application_close_date}
                        maxDate={new Date(this.state.application_close_date).setDate(new Date(this.state.application_close_date).getDate() + 90)}
                        minDate={new Date(this.state.application_close_date).setDate(new Date(this.state.application_close_date).getDate() - 29)}


                    />
                    <DatePicker
                        disabled={this.state.disabled}
                        onChange={(item) => { this.setState({ earliest_start_date: new Date(item) }) }}
                        selected={this.state.earliest_start_date}


                    />
                    <DatePicker
                        disabled={this.state.disabled}
                        onChange={(item) => { this.setState({ latest_end_date: new Date(item) }) }}
                        selected={this.state.latest_end_date}
                        minDate={new Date(this.state.earliest_start_date).setDate(this.state.earliest_start_date.getDate() + 7 * 6)}
                        maxDate={new Date(this.state.earliest_start_date).setDate(this.state.earliest_start_date.getDate() + 7 * 78)}
                    />

                    {/* <TextField
                        disabled={this.state.disabled}
                        id="outlined-disabled"
                        label="Description"
                        value={this.state.data.description}
                        style={styles.textStyle}
                        margin="normal"
                        variant="outlined" /> */}

                    <FormControl >
                        <Select
                            disabled={this.state.disabled}
                            multiple
                            value={this.state.backgrounds}
                            onChange={(event) => {
                                if (event.target.value.length > 3) {

                                }
                                else {
                                    this.setState(
                                        {
                                            backgrounds: event.target.value

                                        }
                                    )
                                }

                            }}
                            input={<Input id="select-multiple-chip" />}
                            renderValue={selected => (
                                <div>
                                    {selected.map(value => (
                                        <Chip key={value} label={value} />
                                    ))}
                                </div>
                            )}
                            MenuProps={MenuProps}
                        >
                            {this.state.backgrounds.map(item => (
                                <MenuItem key={item.id} value={item.name} >
                                    {"kk"}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    {/*<TextField
                        disabled={this.state.disabled}
                        id="outlined-disabled"
                        defaultValue={this.state.data.role_info.selection_process}
                        label="Selection Process"
                        style={styles.textStyle}
                        margin="normal"
                        variant="outlined" />*/}
                    {/* <TextField
                        disabled={this.state.disabled}
                        id="outlined-disabled"
                        defaultValue={this.state.data.specifics_info.salary}
                        label="Salary"
                        style={styles.textStyle}
                        margin="normal"
                        variant="outlined" /> */}
                    {/* <TextField
                        disabled={this.state.disabled}
                        id="outlined-disabled"
                        defaultValue={this.state.data.role_info.city}
                        label="Role Information"
                        style={styles.textStyle}
                        margin="normal"
                        variant="outlined" /> */}

                </div>

            </div>
        )

    }
}


