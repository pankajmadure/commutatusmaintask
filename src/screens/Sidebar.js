import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Button, Row, Col } from 'antd';
import Axios from 'axios';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';


export default class Sidebar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],
            isLoading:true
        }
        this.handleList=this.handleList.bind(this);
    }

    componentDidMount() {
        var params = {
            access_token: "dd0df21c8af5d929dff19f74506c4a8153d7acd34306b9761fd4a57cfa1d483c"
        }
        Axios.get("http://gisapi-web-staging-1636833739.eu-west-1.elb.amazonaws.com/v2/opportunities", { params }
        )
            .then(res => {
                console.log(res.data.data)
                this.setState({
                    data: res.data.data,
                    isLoading: false
                })
            })
            .catch(err => {
                console.log("Some problem with network ");
                this.setState({
                    isLoading: false
                })
            })
    }

    handleList(item) {
        this.props.recievedData(item);

    }
    render() {
        return (
            <div>
                <br /><br /><br />
                {this.state.isLoading
                    ?
                    <div></div>
                    :

                    <Row >
                        <div>
                            <Col sm={2} ></Col>
                            <Col sm={20} >
                                <List style={{ width: "100%", textAlign: "center" }}>
                                    {this.state.data.map((item, index) => {
                                        
                                        return (
                                            <div key={index} style={{ paddingBottom: 8, paddingTop: 8 }} onClick={()=>{this.handleList(item)}}>
                                                <span >
                                                    <h4>{item.title}</h4>

                                                </span>
                                                <Divider variant="inset" component="li" style={{alignItems:'center',alignContent:'center'}} />


                                            </div>
                                        )
                                    })}

                                </List>

                            </Col>
                        </div>
                    </Row>
                }
            </div>
        );
    }
}

