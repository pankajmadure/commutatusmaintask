import React, { Component } from 'react';

import 'antd/dist/antd.css';
import { Divider } from 'antd';


class Header extends Component {
    render() {
        return (
            <div>

                <span style={{ fontSize: '36px', color: 'blue' }}> Header </span>
                <br></br>
                <Divider></Divider>
            </div>
        );
    }
}

export default Header;